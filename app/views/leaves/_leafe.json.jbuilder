json.extract! leafe, :id, :subject, :body, :from_date, :to_date, :acceptable_type, :acceptable_id, :created_at, :updated_at
json.url leafe_url(leafe, format: :json)
